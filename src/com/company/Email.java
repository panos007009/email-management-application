package com.company;

import java.util.Scanner;

public class Email {
    private String firstName;
    private String lastName;
    private String password;
    private String department;
    private int mailboxCapacity = 500;
    private String alternateEmail;
    private String email;
    private String companySuffix = "company.com";

    public String getPassword() {
        return password;
    }

    public int getMailboxCapacity() {
        return mailboxCapacity;
    }

    public String getAlternateEmail() {
        return alternateEmail;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setMailboxCapacity(int mailboxCapacity) {
        this.mailboxCapacity = mailboxCapacity;
    }

    public void setAlternateEmail(String alternateEmail) {
        this.alternateEmail = alternateEmail;
    }

    //Constructor to receive the first and last name
    public Email(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        System.out.println("EMAIL CREATED : " + this.firstName + " " + this.lastName);

        //Calling method to ask and set the department
        this.department = setDepartment();

        //Calling method that returns a random password
        this.password = randomPassword(8);

        //Combine elements to generate email
        email = firstName.toLowerCase() + "." + lastName.toLowerCase() + "@" + department + "." + companySuffix;

    }

    //Method for asking and setting the department
    private String setDepartment() {
        System.out.println("Enter your department\n1 for Sales\n2 for Development\n3 for Accounting\n0 for none");
        Scanner input = new Scanner(System.in);
        int departmentChoice = input.nextInt();
        if (departmentChoice == 0) return "none";
        else if (departmentChoice == 1) return "sales";
        else if (departmentChoice == 2) return "dev";
        else if (departmentChoice == 3) return "acct";
        else {
            System.out.println("Department setting was unsuccessful, please check the input value and try again!");
            return null;
        }
    }

    //Generate random password
    private String randomPassword(int length) {
        String passwordSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstyvwxyz0123456789!@#$%&";
        char [] password = new char[length];
        for (int i = 0; i < length; i++) {
            int rand = (int) (Math.random() * passwordSet.length());
            password[i] = passwordSet.charAt(rand);
        }
        return new String(password);
    }

    public String showInfo () {
        return "DISPLAY NAME : " + firstName + " " + lastName +
                "\nCOMPANY EMAIL : " + email +
                "\nMAILBOX CAPACITY : " + mailboxCapacity + "mb" +
                "\nCURRENT PASSWORD : " + password;
    }
}
